import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import todoAppView from '../views/todoAppView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/todoApp',
    name: 'todoApp',
    component: todoAppView
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
